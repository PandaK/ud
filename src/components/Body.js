import React from 'react';
import { Link } from 'react-router-dom';
//port Portfolio from './Portfolio';
import '../App.scss';



const Body = () => {

    return(
        <>
            <section className="about-us-area text-white">
                <div className="container text-center">
                    <div className="row align-items-center">
                        <div className="col-12">
                            <div className="about-us-text-area mb-5 mt-5">
                                <h2 className="about-us-text title-font-3">
                                A simple API and dashboard giving you access to demographic and  
                                    <br/>
                                    socioeconomic data of your urban area.
                                </h2>
                                <p className="mt-4 mb-4 title-font-2">
                                If you value easy access to information and don’t want to search around the internet anymore, this product is for you. 
                                    <br/>
                                    You can connect to the data in Excel, Google Sheets, Power BI or any other tool that you like.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            
            

            <section className="lead">
                <div className="container text-center">
                    <div className="row align-items-center">
                        <div className="col-12">
                            <div className="lead-title text-white mb-5 mt-4">
                                <h1 className="title-font">
                                Let's work together
                                </h1>
                                <p className="mt-4 mb-4 title-font-2">
                                Sign up now to get one year free access. Available until February 28th, after that  USD 100 per user per month.
                                </p>
                                <Link to="/contact">
                                    <button className="btn text-white">
                                        Contact
                                    </button>
                                </Link>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </>
    );
}

export default Body;