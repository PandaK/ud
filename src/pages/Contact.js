import React from 'react';

const Contact = () => {
    return(
        <>
            <div className="contact section-title mt-5">
                <div className="container text-white">
                    <div className="row align-items-center">
                        <div className="col-md-7 mx-auto">
                            <div className="contact-title mb-5 mt-5">
                                <h1 className="title-font title-font-size">
                                Contact
                                </h1>
                                <p className="mt-4 mb-4 title-font-2">
                                    Say Hello. If you want to extend some info, 
                                    do not hesitate to fill this form, we love to 
                                    say ‘Hello Mate’.
                                </p>
                            </div>
                        </div>

                        <div className="col-lg-8 mx-auto text-white">
                            <div className="contact-form mb-5 mt-5">
                                <form action="mailto:pandimensional11@gmail.com" method="post" enctype="text/plain">
                                    
                                    <div className="col-12 text-center">
                                        <button type="submit" className="btn mt-5">
                                            Send Email
                                        </button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </>
    );
}

export default Contact;